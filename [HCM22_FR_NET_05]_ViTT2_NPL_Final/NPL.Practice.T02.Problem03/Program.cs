﻿using System;

namespace NPL.Practice.T02.Problem03
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student() { ID = 1, Name = "Peter", StartDate = DateTime.Now, SqlMark = 8, CsharpMark = 7, DsaMark = 6 };
            Console.WriteLine(student.GetCertificate()); 
        }
    }
}
