﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPL.Practice.T02.Problem03
{
    public class Student : IGraduate
    {
        private int id;
        private string name;
        private DateTime startDate;
        private decimal sqlMark;
        private decimal csharpMark;
        private decimal dsaMark;
        private decimal gPA;
        private GraduateLevel graduateLevel;

        public Student()
        {

        }

        public Student(int Id, string Name, DateTime StarDate, decimal SqlMark, decimal CsharpMark, decimal DsaMark, decimal GPA)
        {
            id = Id;
            name = Name;
            startDate = StarDate;
            sqlMark = SqlMark;
            csharpMark = CsharpMark;
            dsaMark = DsaMark;
            gPA = GPA;
        }
        internal enum GraduateLevel
        {
            Excellent,
            VeryGood,
            Good,
            Average,
            Failed
        }
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        public decimal SqlMark
        {
            get { return sqlMark; }
            set { sqlMark = value; }
        }

        public decimal CsharpMark
        {
            get { return csharpMark; }
            set { csharpMark = value; }
        }

        public decimal DsaMark
        {
            get { return dsaMark; }
            set { dsaMark = value; }
        }

        public decimal GPA
        {
            get { return gPA; }
            set { gPA = value; }
        }


        public decimal GetGPA()
        {
            return (SqlMark + CsharpMark + DsaMark) / 3; 
        }


        public void Graduate()
        {
            
            decimal GPA = (sqlMark + csharpMark + dsaMark) / 3;
            if (gPA >= 9)
            {
                graduateLevel = GraduateLevel.Excellent;
            }
            else if (GPA >= 8 && GPA < 9)
            {
                graduateLevel = GraduateLevel.VeryGood;
            }
            else if (GPA >= 7 && GPA < 8)
            {
                graduateLevel = GraduateLevel.Good;
            }
            else if (GPA >= 5 && GPA < 7)
            {
                graduateLevel = GraduateLevel.Average;
            }
            else if (GPA < 5)
            {
                graduateLevel = GraduateLevel.Failed;
            }
        }


        public string GetCertificate()
        {
            Console.WriteLine("Thông tin sinh viên");
            //string GraduateLevel = 
            return $"Name: {name},\tSqlMark: {sqlMark},\t CsharpMark: {csharpMark},\tDsaMark: {dsaMark},\tGPA: {GetGPA()}, GraduateLevel: {graduateLevel}";

        }
    }
}
