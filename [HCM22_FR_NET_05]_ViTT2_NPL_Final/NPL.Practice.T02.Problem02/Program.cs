﻿using System;

namespace NPL.Practice.T02.Problem02
{
    class Program
    {



        static public int FindMaxSubArray(int[] inputArray, int subLength)
        {

            int result = 0;
            try
            {
                if (inputArray.Length == 0)
                {
                    Console.WriteLine("Mảng không có giá tri");
                    return 0;
                }
                else
                {
                    for (int i = 0; i < inputArray.Length - subLength; i++)
                    {
                        int sum = 0;
                        for (int j = i; j <= i + subLength; j++)
                        {
                            sum += inputArray[i];
                            if (result < sum)
                            {
                                result = sum;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Loi: {ex.Message}");
            }
            return result;
        }
        static void Main(string[] args)
        {

            int[] inputArray = new int[] { 1, 2, 5, -4, 3 };
            Console.WriteLine("Nhập độ dài mảng con: ");
            int subLenght = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(FindMaxSubArray(inputArray, subLenght));
            Console.ReadKey();
        }
    }
}
