﻿using System;

namespace ViTT2_NPL_Final
{
    class Program
    {
        static public string GetArticleSummary(string content, int maxLength)
        {
            try
            {
                string threeDot = "...";
                content = content.Substring(0, maxLength);
                content = String.Concat(content, threeDot);
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return content;
        }
        static void Main(string[] args)
        {
            Console.InputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Nhập chuỗi: ");
            string content = Console.ReadLine();
            Console.WriteLine("Nhập độ dài tối đa: ");
            int maxLenght = Convert.ToInt32(Console.ReadLine());
            string newContent = GetArticleSummary(content, maxLenght);
            Console.WriteLine(newContent);
            Console.ReadKey();
        }
    }
}
